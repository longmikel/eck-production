# ECK for Production

<div align="center">
  <a href="https://www.elastic.co/cloud/"><img alt="Elastic Cloud" src="./media/elastic-cloud.png" height="140" /></a>
  <br />
  <strong>Elastic Cloud</strong>

  <div align="center">
  <a href="https://www.elastic.co/guide/en/cloud-on-k8s/current/index.html"><img src="https://img.shields.io/badge/Documentation-0078D4?style=for-the-badge&logo=gitbook&logoColor=d9e0ee&labelColor=302d41&color=7dc4e4"></a>
  <a href="https://twitter.com/elastic"><img src="https://img.shields.io/badge/%40Elastic-0078D4?style=for-the-badge&logo=Twitter&logoColor=d9e0ee&labelColor=302d41&color=eed49f"></a>
  <a href="https://discuss.elastic.co/"><img src="https://img.shields.io/badge/Discord%20Server-0078D4?style=for-the-badge&logo=discord&logoColor=d9e0ee&labelColor=302d41&color=cba6f7"></a>
  <a href="https://github.com/elastic/cloud-on-k8s/releases/latest"><img src="https://img.shields.io/github/v/release/elastic/cloud-on-k8s.svg?style=for-the-badge&logo=Github&logoColor=d9e0ee&labelColor=302d41&color=8bd5ca"></a>
  </div>
</div>

<div align="center">
  <a href="https://www.elastic.co">Website</a>
  ·
  <a href="#-configuration">Configuration</a>
  ·
  <a href="#-installation">Installation</a>
</div>

<div align="center">
  <a href="./README.md"
    ><img
      height="20"
      src="./media/flag-us.png"
      alt="English"
  /></a>
  &nbsp;
  <a
    href="./docs/vi-VN/README.md"
    ><img
      height="20"
      src="./media/flag-vn.png"
      alt="Tiếng Việt"
  /></a>
</div>

***

## 🔥 What is ECK?
Elastic Cloud on Kubernetes (ECK) automates the deployment, provisioning, management, and orchestration of Elasticsearch, Kibana, APM Server, Enterprise Search, Beats, Elastic Agent, and Elastic Maps Server on Kubernetes based on the operator pattern.

## 📖 Documentation & FAQ
You can check out the documentation at [**elastic.co/guide/en/cloud-on-k8s/**](https://www.elastic.co/guide/en/cloud-on-k8s/) and the Frequently Asked Questions at [**here**](https://www.elastic.co/guide/en/cloud/current/ec-faq-getting-started.html) for more information.

## 📦 Install And Configure ECK using the YAML manifest
### Prerequisites
1. [x] A Kubernetes Cluster with Kubeconfig file for Helm to access your Kubernetes Cluster (~/.kube/config). Kubernetes Version Support the following below.
2. [x] CustomResourceDefinition objects for all supported resource types (Elasticsearch, Kibana, APM Server, Enterprise Search, Beats, Elastic Agent, and Elastic Maps Server).
3. [x] Namespace named elastic-system to hold all operator resources.
4. [x] ServiceAccount, ClusterRole and ClusterRoleBinding to allow the operator to manage resources throughout the cluster.
5. [x] ValidatingWebhookConfiguration to validate Elastic custom resources on admission.
6. [x] StatefulSet, ConfigMap, Secret and Service in elastic-system namespace to run the operator application.

Due to changes in CRD version support, the following versions of the chart are usable and supported on the following Kubernetes versions:

|                         | Kubernetes v1.15 and below | Kubernetes v1.16-v1.21 | Kubernetes v1.22 and above |
|-------------------------|----------------------------|------------------------|----------------------------|
| Chart v9.20.2 and below | <center>[x]</center>       | <center>[x]</center>   |                            |
| Chart 10.0.0 and above  |                            | <center>[x]</center>   | <center>[x]</center>       |

<a name="-configuration"></a>

## ❄️  Configuration
ECK have the following requirement to function:
- Custom resource definitions.
- Operator with RBAC.
- License with Basic or Enterprise (options).
- Storage Class (Hot, Warm, Cold and Frozen).
- Monitoring ES.
- Elasticsearch resource autoscaling.
- Kibana resource.
- Fleet resource with kubernetes integration (options).
- Heartbeat resource (options).
- External-dns with Cloudflare integration (options).
- Certificate with let's encrypt integration.
- Ingress with nginx resource.
- SAML with auth0 (options)

All configuration for ECK in directory manifests is done this `YAML` file. There are some [**examples**](./manifests/).

> Remember you must have basic or enterprise license to run ECK.

<a name="-installation"></a>

## 🚀 Installation
### Step 1: Cloning a repository [**eck-production**](https://gitlab.com/longmikel/eck-production.git)
``` sh
git clone https://gitlab.com/longmikel/eck-production.git
```

### Step 2: Create a kubernetes namespace
You can replace namespace `elastic-system` to namespace another.

``` sh
kubectl create namespace elastic-system
```

### Step 3: Install custom resource definitions
``` bash
kubectl apply -f ./manifests/001-eck-crds.yaml -n elastic-system
```

### Step 4: Install the operator with its RBAC rules
``` bash
kubectl apply -f ./manifests/002-eck-operator.yaml -n elastic-system
```

### Step 5: Download your license (Order license on elastic.co) and apply it via secret (or apply the license.yaml)
``` bash
kubectl apply -f ./manifests/003-eck-license.yaml -n elastic-system
```

### Step 6: Create dedicated storage class by apply 004-eck-storageclass-hot.yaml and 005-eck-storageclass-warm.yaml
If the storage class already exists, you can skip this step or specify a new storage class for elastic cloud applications.

### Step 7: Create the monitoring cluster (it will create a ns call monitoring) by applying 006-eck-monitoring-es.yaml
``` bash
kubectl apply -f ./manifests/006-eck-monitoring-es.yaml -n elastic-system
```

### Step 8: Create an elasticsearch cluster by creating an elasticsearch resource
``` bash
kubectl apply -f ./manifests/007-eck-elasticsearch.yaml -n elastic-system
```

### Step 9: Create kibana resource
``` bash
kubectl apply -f ./manifests/008-eck-kibana.yaml -n elastic-system
```

### Step 10: Create fleet resource
``` bash
kubectl apply -f ./manifests/009-eck-fleet.yaml -n elastic-system
```

### Step 11: Create heartbeat resource
``` bash
kubectl apply -f ./manifests/010-eck-heartbeat.yaml -n elastic-system
```

### Step 12: Create metricbeat resource
``` bash
kubectl apply -f ./manifests/011-eck-metricbeat_hosts.yaml -n elastic-system
```

### Step 13: Create packetbeat resource
``` bash
kubectl apply -f ./manifests/012-eck-packetbeat.yaml -n elastic-system
```

### Step 14: Create external-dns
``` bash
kubectl apply -f ./manifests/013-eck-external-dns.yaml -n elastic-system
```

### Step 15: Create let's encrypt certificate
``` bash
kubectl apply -f ./manifests/014-eck-certificate.yaml -n elastic-system
```

### Step 16: Create Ingress for kibana service and elasticsearch service with nginx resource
- Ingress for kibana service 
``` bash
kubectl apply -f ./manifests/015-eck-ingress-kb.yaml -n elastic-system
```

- Ingress for elasticsearch service
``` bash
kubectl apply -f ./manifests/016-eck-ingress-es.yaml -n elastic-system
```

## Accessing
For this example, I am using a domain call 2kz.asia and as I am using external-dns, the DNS entry will be automatically added to Cloudflare, https://ssa.2kz.asia and https://monitoring-ssa.2kz.asia. You can check the connection is safe and we are using a valid certificate by let's encrypt.

If you want to login using SAML, make sure to adjust the saml session on elasticsearch.yml according to your environment.
